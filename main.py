import requests


def fun(x):
    return x + 1


def get_data():
    # funcion consultar una api
    url = "https://api.chucknorris.io/jokes/random"
    data = requests.get(url)
    return data.json()
    # return {"categories": "hola mundo", "value": "sdf"}
