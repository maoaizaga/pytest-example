
def factorial(numero):
    if not isinstance(numero, int):
        raise Exception("Por favor ingresa un número entero")
    elif numero < 0:
        raise Exception("Por favor ingresa un número entero positivo")
    elif numero == 0:
        return 1
    fact = 1
    for i in range(1, numero + 1):
        fact = fact * i
    return fact
