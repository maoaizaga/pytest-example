from main import fun, get_data


def test_fun_returns_correct_value():
    resp = fun(3)
    assert resp == 4


def test_fun_returns_incorrect_value():
    resp = fun(3)
    assert resp != 5


def test_get_data_returns_a_dict(mocker):
    """
    añsdlkfjañdslkasñdlfk
    """
    mock_request_get = mocker.patch("requests.get")

    response = {
        'categories': [],
        'value': 'It is impossible for Chuck Norris to get skidmarks because he craps bleach'
    }
    mock_data = mocker.MagicMock()
    mock_data.json.return_value = response

    mock_request_get.return_value = mock_data

    resp = get_data()
    assert resp.keys() == response.keys()


def test_get_data_calls_request_get(mocker):
    mock_request_get = mocker.patch("requests.get")

    get_data()

    mock_request_get.assert_called()
