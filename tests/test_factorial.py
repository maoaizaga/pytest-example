import pytest

from src.maths import factorial


def test_receives_one_parameter():

    with pytest.raises(TypeError) as e:
        factorial()

    assert "factorial() missing 1 required positional argument:" in str(e.value)


def test_exception_when_parameter_is_not_an_integer():
    with pytest.raises(Exception) as e:
        factorial("-1")

    assert str(e.value) == "Por favor ingresa un número entero"


def test_exception_when_parameter_is_a_positive_integer():
    with pytest.raises(Exception) as e:
        factorial(-1)

    assert str(e.value) == "Por favor ingresa un número entero positivo"


def test_base_case():
    """Verifies the 0 factorial is 1"""
    resp = factorial(0)
    assert resp == 1


def test_n_number():
    resp = factorial(5)
    assert resp == 120
    resp = factorial(1)
    assert resp == 1
    resp = factorial(3)
    assert resp == 6
