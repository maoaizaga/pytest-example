from prompt import prompt
from unittest.mock import patch


@patch('src.maths.factorial', return_value=5)
@patch("builtins.input")
def test_factorial_is_called(mock_input, mock_factorial):
    mock_input.return_value = 3

    prompt()

    mock_factorial.assert_called()
