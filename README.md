# pytest-example

## Dependencies
* Pyenv https://github.com/pyenv/pyenv#installation (optional)
* Pipenv https://pipenv.readthedocs.io/en/latest/

## Getting started

```shell
pyenv install 3.9.13
pyenv local 3.9.13
pipenv sync
pipenv shell

pytest test.py
```